import http.client
import json

#Insert link
link = "xrwxk3.api.infobip.com"

#Insert authorization key
auth_key = "79cd5dc3310f2ec87c7b3b98bc8d70ff-ca5aaedb-972a-4741-9ac4-4eee9f772258"

#Insert message recipients
number = ["639231764086","639997878149"]

#Create the headers
headers = {
    'Authorization': 'App '+auth_key,
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}

#Create http connection
conn = http.client.HTTPSConnection(link)

#Create the message
message = '''Test Logic
'''

for i,v in enumerate(number):
    #Create the payload
    payload = {
      "messages": [
        {
          "from": "Komunidad",
          "destinations": [
            {
              "to": v
            }
          ],
          "text": message
        }
      ]
    }
    
    #Send the sms     
    conn.request("POST", "/sms/2/text/advanced", json.dumps(payload), headers)
    res = conn.getresponse()
    data = res.read()
    print(data.decode("utf-8"))
