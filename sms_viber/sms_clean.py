import http.client
from utils import send_sms_info

#Insert message recipients
number = ["639454091790","639988451451"]

#Insert link
link = "xrwxk3.api.infobip.com"

#Insert authorization key
auth_key = "79cd5dc3310f2ec87c7b3b98bc8d70ff-ca5aaedb-972a-4741-9ac4-4eee9f772258"

#Create http connection
conn = http.client.HTTPSConnection(link)

#Create the headers
headers = {
    'Authorization': 'App '+auth_key,
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}

#Create the message
message = '''
Test Logic
'''

for i,v in enumerate(number):
    #i = 0 
    #v = number[i]
    #Send sms using infobip SMS API
    send_sms_info(conn,headers,v,message)

import json

#Send sms using infobip SMS API
def send_sms_info(conn,headers,number,message):
    #Create the payload
    payload = {
      "messages": [
        {
          "from": "Komunidad",
          "destinations": [
            {
              "to": number
            }
          ],
          "text": message
        }
      ]
    }
    
    #Send the sms     
    conn.request("POST", "/sms/2/text/advanced", json.dumps(payload), headers)
    res = conn.getresponse()
    data = res.read()
    print(data.decode("utf-8"))
    return

