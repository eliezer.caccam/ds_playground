import requests
from dateutil import parser

#Insert location
lat = 14.5
lon = 121
location = "Manila"

#Insert api_key
api_key = "900dccdd95e347f38dccdd95e3b7f39f"

#Add link
link = "https://api.weather.com/v3/wx/observations/current?geocode="+str(lat)+","+str(lon)+"&units=m&language=en-US&format=json&apiKey="+api_key

#Insert time format
fmt = "%b %d, %Y (%a) %I:%M:%S %p"

#Create sessions
s = requests.Session()

#Call the api
data = (s.get(link)).json()

#Pull the needed data
time = data["validTimeLocal"]
time_edited = (parser.parse(time)).strftime(fmt)
temperature = data["temperature"]
heat_index = data["temperatureHeatIndex"]
wind_speed = data["windSpeed"]
wind_direction = data["windDirectionCardinal"]

#Create a subject and body
subject = "Weather Status for "+location
body = """Weather Status for """+location+""" ("""+str(lat)+"""N, """+str(lon)+"""E)
As of """+time_edited+""", the current temperature is """+str(temperature)+""" Deg C but it feels like """+str(heat_index)+""" Deg C.
The winds are coming from """+wind_direction+""" at a speed of """+str(wind_speed)+""" kph.
"""
print (body)