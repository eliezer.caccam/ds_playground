import requests
from dateutil import parser
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

#Get the latest weather status
def wx_stat_info(lat,lon,location,api_key):
    #Add link
    link = "https://api.weather.com/v3/wx/observations/current?geocode="+str(lat)+","+str(lon)+"&units=m&language=en-US&format=json&apiKey="+api_key
    
    #Insert time format
    fmt = "%b %d, %y %I:%M:%S %p"
    
    #Create sessions
    s = requests.Session()
    
    #Call the api
    data = (s.get(link)).json()
    
    #Pull the needed data
    time = data["validTimeLocal"]
    time_edited = (parser.parse(time)).strftime(fmt)
    temperature = data["temperature"]
    heat_index = data["temperatureHeatIndex"]
    wind_speed = data["windSpeed"]
    wind_direction = data["windDirectionCardinal"]
    
    #Create a sentence
    subject = "Weather Status for "+location
    body = """<html>
<head>
<style>
h3 {
    font-family: Tahoma;    
}
p {
    font-family: Tahoma;    
}
</style>
</head>
<body>
<h3>Weather Status for """+location+""" ("""+str(lat)+"""N, """+str(lon)+"""E)</h3>
<p>As of """+time_edited+""", the current temperature is """+str(temperature)+""" Deg C but it feels like """+str(heat_index)+""" Deg C.
The winds are coming from """+wind_direction+""" at a speed of """+str(wind_speed)+""" kph.</p>
</body>
</html>
"""
    return (subject,body)

#Send email
def send_email_info(send_email,receive_email,pw,subject,body):
    #Set the server
    server_ssl = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server_ssl.ehlo()
    
    #Log in to the server
    server_ssl.login(send_email,pw)
    
    #Send the email
    receive_email = ",".join(receive_email)
    msg = MIMEMultipart() 
    msg["From"] = send_email
    msg["Subject"] = subject
    msg["To"] = receive_email

    #Attach the body of the email
    msg.attach(MIMEText(body, "html"))
    
    #Add the data
    text = msg.as_string()
    server_ssl.sendmail(send_email,receive_email.split(","), text)
    server_ssl.quit()
    return    