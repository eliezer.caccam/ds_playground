import pandas as pd
from email_playground.utils import send_email_info

#Insert path
path = "input/"

#Set email input
send_email = "jong.lequiron@komunidad.co"
pw = "wiyvauuubscnebet"

#Insert receive_email
receive_email = ["jong.lequiron@komunidad.co","john.christian.b.lequiron@gmail.com"]

#Read the dataframe
data = pd.read_excel(path+"location.xlsx")

#Convert dataframe to html
data_1 = data.to_html(header = True,index = None)

#Finalize the body (No CSS)
subject = "Test Table"
body = """<html>
<body>"""+data_1+"""</body>
</html>
"""

#Finalize the body (With CSS)
data_2 = data_1.replace('''<table border="1" class="dataframe">''','''<table class="center">''')
data_3 = data_2.replace('''<td>No Warning</td>''','''<td class="no_warning">No Warning</td>''')
data_4 = data_3.replace('''<td>Monitor</td>''','''<td class="monitor">Monitor</td>''')
data_5 = data_4.replace('''<td>Watch</td>''','''<td class="watch">Monitor</td>''')
subject = "Test Table"
body = """<html>
<head>
<style>
.center {
  margin-left: auto;
  margin-right: auto;
  border-collapse: collapse;
  font-family: Tahoma;
  width: 100%;
    }
td, th, tr {
    padding: 3px;
    text-align: center;
    border: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    font-family: Tahoma;
    }
th {
    background-color: #2a3457;
    color: white;
    }
td.no_warning {
    background-color: green;
    color: white;
    }
td.monitor {
    background-color: yellow;
    color: black;
    }
td.watch {
    background-color: orange;
    color: white;
    }
td.warning {
    background-color: red;
    color: white;
    }
</style>
</head>
<body>
"""+data_5+"""
</body>
</html>
"""

#Send email
send_email_info(send_email,receive_email,pw,subject,body)